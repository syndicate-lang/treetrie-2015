#ifndef ROUTE_H_c2d88c54_f18d_4b71_9fce_5d43c0723360
#define ROUTE_H_c2d88c54_f18d_4b71_9fce_5d43c0723360

/* Sticking to odd-numbered constants, since the plan is for most
   bindings to use odd-numbered atoms for immediates and even-numbered
   atoms for heap-allocated complex values. TODO: make this a proper
   part of the interface? */
#define TT_WILD ((tt_atom_t) -1)	/* Wildcard */
#define TT_BOS ((tt_atom_t) -3)		/* Beginning of sequence */
#define TT_EOS ((tt_atom_t) -5)		/* End of sequence */
#define TT_BOC ((tt_atom_t) -7)		/* Beginning of capture */
#define TT_EOC ((tt_atom_t) -9)		/* End of capture */

/* N.B. Returns a tt_grab'd result. */
extern tt_node_ptr_t tt_trie_combine(tt_arena_t *a,
				     tt_node_ptr_t r1,
				     tt_node_ptr_t r2,
				     int left_empty_keep,
				     int right_empty_keep,
				     int left_base_keep,
				     int right_base_keep,
				     void *f_context,
				     /* Should return a tt_grab'd result. */
				     tt_node_ptr_t (*f)(void *f_context,
							tt_node_ptr_t r1,
							tt_node_ptr_t r2));

/* N.B. Returns a tt_grab'd result. */
extern tt_node_ptr_t tt_trie_union_map(tt_arena_t *a, tt_node_ptr_t r1, tt_node_ptr_t r2);
extern tt_node_ptr_t tt_trie_union_set(tt_arena_t *a, tt_node_ptr_t r1, tt_node_ptr_t r2);
extern tt_node_ptr_t tt_trie_subtract_set(tt_arena_t *a, tt_node_ptr_t r1, tt_node_ptr_t r2);

/* ungrab'd */
extern tt_node_ptr_t tt_trie_step(tt_arena_t *a, tt_node_ptr_t r, tt_atom_t key);

/* ungrab'd */
extern tt_node_ptr_t tt_trie_relabel(tt_arena_t *a,
				     tt_node_ptr_t r,
				     void *f_context,
				     tt_node_ptr_t (*f)(void *f_context,
							tt_node_ptr_t oldlabel));
extern tt_node_ptr_t tt_trie_relabel_const(tt_arena_t *a, tt_node_ptr_t r, tt_node_ptr_t newlabel);

/* These return ungrab'd */
extern tt_node_ptr_t tt_begin_path(tt_arena_t *a, tt_node_ptr_t ok_dict);
extern tt_node_ptr_t tt_prepend_path(tt_arena_t *a, tt_atom_t tok, tt_node_ptr_t tail);

extern void tt_dump_routingtable(tt_arena_t *a, tt_node_ptr_t r, int initial_indent);

// TODO move to the right place
#ifdef __cplusplus
extern "C" {
#endif

#ifdef __cplusplus
}
#endif

#endif
