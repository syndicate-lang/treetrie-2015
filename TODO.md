Consider whether a stack or a queue makes most sense for the freelist.

Consider a naming convention to indicate grab'd vs ungrab'd results.


Needed by prospect:

 - `pattern->matcher` with `#t`

 - `matcher-project`
 - `matcher-project/set`
 - `matcher-project/set/single`

     - many userland uses
     - one interesting use in `drop-interests` / `drop-patch`, used in `mux-update-stream`
        - perhaps this can be replaced with a `matcher-step` combination?
        - no, it'd be a little awkward, because of the trailing close-paren.

 - `matcher-match-value`

     - used to route messages
     - once to check that noone has asserted the message in the routing table
     - and once to actually route it

 - `matcher-match-matcher`

     - used in `compute-affected-pids` to project out PIDs on the
       right (after a step to get past the struct:observe)
     - used with a combiner that unions in the PIDs on the right
     - used with a left-short that steps over the close-paren from the struct:observe

 - `matcher-union` with default and with `\vw.#t`

     - used to create cross-level observations in `observe-at-meta` in core
     - to compute the "cover" of a patch in compute-affected-pids -
       union of added & removed
     - used in `apply-patch` and `patch-union` with default combiner
     - used in `update-interests` and `compose-patch` with a combiner
       that ignores the structure of the ok() value
     - (used in `unapply-patch`, which is not used)

 - `matcher-step` (for removing open-paren and observation marks and
   skipping the corresponding close-paren)

     - used in `biased-intersection` and `compute-affected-pids` to
       skip struct:observe

 - `matcher-relabel`

     - used throughout to replace ok() leaf values.

 - `matcher-subtract` with default, `\vw.#f`, `add-combiner` and `rem-combiner`

     - used with always-false in `limit-patch`, `compose-patch` and
       `update-interests`, ignoring the ok() values
     - used with default in `compute-patch` (and in `limit-patch/routing-table`, which is not used)
     - used with both `add-combiner` and `rem-combiner` in `compute-aggregate-patch`
     - used with default in `apply-patch`
     - (used in `unapply-patch`, which is not used)

 - `matcher-intersect` with `\vw.v`, `\vw.v∩w`, and `\vw.#t` with a left-short argument.

     - used in `limit-patch` in a way that's tantamount to an explicit simultaneous relabelling
     - used with an always-true combiner in `biased-intersection`
     - (used in `limit-patch/routing-table`, which is unused)


    digraph Dependencies {
		graph [rankdir=LR];

		// matcher
		subgraph xcluster_union {
		  unionUnit;
		  unionPIDs;
	    }
        project;
        matchValue;
        matchMatcher;
        step;
        relabel;
		subgraph xcluster_subtract {
		  subtractUnit;
          subtractPIDs;
		  subtractCustom;
        }
		subgraph xcluster_intersect {
		  intersectAsymmetric;
		  intersectUnit;
        }

        // patch
		// computePatch -> subtractUnit; // I guess?
		// stripInterests -> relabel;
		// stripPatch -> stripInterests;
		applyPatch -> subtractPIDs;
		applyPatch -> unionPIDs;
		biasedIntersection -> intersectUnit;
		biasedIntersection -> step;
		composePatch -> subtractUnit;
		composePatch -> unionUnit;
		computeAggregatePatch -> subtractCustom;
		labelInterests -> relabel;
		limitPatch -> intersectAsymmetric;
		limitPatch -> subtractUnit;
		patchUnion -> unionUnit;
		updateInterests -> subtractUnit;
		updateInterests -> unionUnit;
        dropInterests -> project;
        labelPatch -> labelInterests;
		patchSeq -> composePatch;

        // mux
		computeAffectedPIDs -> matchMatcher;
		computeAffectedPIDs -> step;
		computeAffectedPIDs -> unionUnit;
		messageRouting -> matchValue;
		muxUpdateStream -> applyPatch;
		muxUpdateStream -> biasedIntersection;
		muxUpdateStream -> computeAffectedPIDs;
		muxUpdateStream -> computeAggregatePatch;
		muxUpdateStream -> dropInterests;
		muxUpdateStream -> labelPatch;
		muxUpdateStream -> limitPatch;
		muxUpdateStream -> patchUnion;

	    // core
		observeAtMeta -> unionUnit;

	    // user code
		userCode -> project;
        userCode -> observeAtMeta;
		userCode -> patchSeq;

        // semantics
        semantics -> muxUpdateStream;
		semantics -> messageRouting;
		groundVM -> applyPatch;
		groundVM -> labelPatch;
		demandMatcher -> updateInterests;
    }
