#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <stdint.h>
#include <assert.h>
#include <sys/time.h>

#include "fasthash.h"
#include "treetrie.h"
#include "critbit.h"
#include "route.h"

#include <arpa/inet.h>
#define LITERAL4(s) ({				\
      int ___v = 0;				\
      memcpy(&___v, s, 4);			\
      ntohl(___v);				\
    })

static void atom_incref(void *atom_context, tt_arena_t *a, tt_atom_t atom) {
  /* printf("incref %u\n", atom); */
}

static void atom_decref(void *atom_context, tt_arena_t *a, tt_atom_t atom) {
  /* printf("decref %u\n", atom); */
}

int main0(int argc, char *argv[]) {
  tt_arena_t a;
  int i, outer;
  tt_node_ptr_t prev = TT_EMPTY;

  setbuf(stdout, NULL);
  tt_arena_init(&a, NULL, atom_incref, atom_decref);

  for (outer = 0; outer < 10; outer++) {
    /* printf("---------------------------------------- grab/drop of %d\n", prev); */
    tt_grab(&a, prev);
    tt_drop(&a, prev);
    /* tt_arena_flush(&a); */
    /* printf("---------------------------------------- AFTER DROP of %d:\n", prev); */
    /* tt_dump_arena(&a, 1); */
    prev = TT_EMPTY;
    /* printf("======================================== LOOP ITERATION %d\n", outer); */
    /* tt_dump_arena_summary(&a); */
    for (i = 0; i < 1000000; i++) {
      tt_node_ptr_t leaf = tt_cons_leaf(&a, TT_EMPTY, 1001);
      tt_node_ptr_t curr = tt_cons_node(&a, 0, leaf, prev);
      /* tt_dump_arena(&a, 1); */
      prev = curr;
    }
  }

  /* tt_dump_arena(&a, 1); */
  tt_grab(&a, prev);
  tt_drop(&a, prev);
  /* tt_arena_flush(&a); */
  tt_dump_arena_summary(&a);

  tt_arena_done(&a);
  return EXIT_SUCCESS;
}

static int dump_mapping(void *context, tt_atom_t key, tt_node_ptr_t trie) {
  printf(" -- %u --> %u/%u\n", key, tt_ptr_idx(trie), tt_ptr_tag(trie));
  return 1;
}

int main1(int argc, char *argv[]) {
  tt_arena_t a;
  tt_node_ptr_t curr = TT_EMPTY_DICT;
  int i;

  setbuf(stdout, NULL);
  tt_arena_init(&a, NULL, atom_incref, atom_decref);

  /* tt_dump_arena(&a, 1); */
  for (i = 0; i < 1000000; i++) {
    tt_node_ptr_t next = tt_grab(&a, tt_dict_set(&a, curr, i, TT_EMPTY));
    tt_drop(&a, curr);
    curr = next;
    /* printf("\nAfter i=%d...\n", i); */
    /* tt_dump_arena(&a, 1); */
  }

  /* for (i = 0; i < 1000000; i++) { */
  /*   tt_node_ptr_t next = tt_grab(&a, tt_dict_remove(&a, curr, i << 1)); */
  /*   tt_drop(&a, curr); */
  /*   curr = next; */
  /* } */

  /* tt_arena_flush(&a); */
  printf("\nFinal tree node index is %u/%u; dict size is %u\n",
	 tt_ptr_idx(curr),
	 tt_ptr_tag(curr),
	 TT_DICT_SIZE(&a, curr));
  /* tt_dump_arena(&a, 1); */

  /* tt_dict_foreach(&a, curr, NULL, dump_mapping); */

  tt_drop(&a, curr);
  curr = TT_NO_PTR;
  /* tt_arena_flush(&a); */
  /* tt_dump_arena(&a, 1); */
  tt_dump_arena_summary(&a);

  tt_arena_done(&a);
  return EXIT_SUCCESS;
}

/* http://wiki.osdev.org/Inline_Assembly/Examples#RDTSC */
static inline uint64_t rdtsc(void)
{
  uint64_t ret;
  asm volatile ( "rdtsc" : "=A"(ret) );
  return ret;
}

static tt_node_ptr_t make_path(tt_arena_t *a, tt_node_ptr_t ok_dict, int n, tt_atom_t const *atoms)
{
  int i;
  tt_node_ptr_t result = tt_begin_path(a, ok_dict);
  for (i = n - 1; i >= 0; i--) {
    result = tt_prepend_path(a, atoms[i], result);
  }
  return result;
}

#define MAKE_PATH(a, ok_dict, elts...) ({				\
      tt_atom_t __atoms[] = elts;					\
      make_path(a, ok_dict, sizeof(__atoms)/sizeof(tt_atom_t), __atoms); \
    })

int main(int argc, char *argv[]) {
  tt_arena_t a;
  tt_node_ptr_t a_set;
  tt_node_ptr_t b_set;

  setbuf(stdout, NULL);
  tt_arena_init(&a, NULL, atom_incref, atom_decref);

  a_set = tt_grab(&a, tt_dict_singleton(&a, 'A', TT_EMPTY));
  b_set = tt_grab(&a, tt_dict_singleton(&a, 'B', TT_EMPTY));

  printf("\n============================================================ 1\n");
  {
    tt_node_ptr_t A = tt_grab(&a, MAKE_PATH(&a, a_set, {TT_BOS, 'A', TT_EOS}));
    printf("\nA node: %u/%u\n", tt_ptr_idx(A), tt_ptr_tag(A));
    tt_arena_flush(&a);
    tt_dump_arena_dot_styled("A", A, &a, TT_STYLE_TEXT_LABELS);
    tt_node_ptr_t B = tt_grab(&a, MAKE_PATH(&a, b_set, {TT_BOS, 'B', TT_EOS}));
    printf("\nB node: %u/%u\n", tt_ptr_idx(B), tt_ptr_tag(B));
    tt_dump_arena_dot_styled("B", B, &a, TT_STYLE_TEXT_LABELS);
    tt_node_ptr_t C = tt_trie_union_map(&a, A, B);
    tt_arena_flush(&a);
    tt_dump_arena_dot_styled("Cpredrop", C, &a, TT_STYLE_TEXT_LABELS);
    tt_drop(&a, A);
    tt_drop(&a, B);
    printf("\nC node: %u/%u\n", tt_ptr_idx(C), tt_ptr_tag(C));
    tt_arena_flush(&a);
    tt_dump_arena_dot_styled("Cpostdrop", C, &a, TT_STYLE_TEXT_LABELS);
    putchar('\n');
    tt_dump_routingtable(&a, C, 0);
    tt_drop(&a, C);
  }

  tt_arena_flush(&a);
  tt_dump_arena_dot_styled("after1", TT_NO_PTR, &a, TT_STYLE_TEXT_LABELS);

  printf("\n============================================================ 2\n");
  {
    tt_node_ptr_t A = tt_grab(&a, MAKE_PATH(&a, a_set, {TT_BOS, 'A', TT_EOS}));
    printf("\nA node: %u/%u\n", tt_ptr_idx(A), tt_ptr_tag(A));
    tt_arena_flush(&a);
    tt_dump_arena_dot_styled("A", A, &a, TT_STYLE_TEXT_LABELS);
    tt_node_ptr_t B = tt_grab(&a, MAKE_PATH(&a, b_set, {TT_BOS, TT_WILD, TT_EOS}));
    printf("\nB node: %u/%u\n", tt_ptr_idx(B), tt_ptr_tag(B));
    tt_dump_arena_dot_styled("B", B, &a, TT_STYLE_TEXT_LABELS);
    tt_node_ptr_t C = tt_trie_union_map(&a, A, B);
    tt_arena_flush(&a);
    tt_dump_arena_dot_styled("Cpredrop", C, &a, TT_STYLE_TEXT_LABELS);
    tt_drop(&a, A);
    tt_drop(&a, B);
    printf("\nC node: %u/%u\n", tt_ptr_idx(C), tt_ptr_tag(C));
    tt_arena_flush(&a);
    tt_dump_arena_dot_styled("Cpostdrop", C, &a, TT_STYLE_TEXT_LABELS);
    putchar('\n');
    tt_dump_routingtable(&a, C, 0);
    tt_drop(&a, C);
  }

  printf("\n============================================================ 3\n");
  {
    tt_node_ptr_t A = tt_grab(&a, MAKE_PATH(&a, a_set, {TT_BOS, 'A', TT_EOS}));
    printf("\nA node: %u/%u\n", tt_ptr_idx(A), tt_ptr_tag(A));
    tt_arena_flush(&a);
    tt_dump_arena_dot_styled("A", A, &a, TT_STYLE_TEXT_LABELS);
    tt_node_ptr_t B = tt_grab(&a, MAKE_PATH(&a, b_set, {TT_WILD}));
    printf("\nB node: %u/%u\n", tt_ptr_idx(B), tt_ptr_tag(B));
    tt_dump_arena_dot_styled("B", B, &a, TT_STYLE_TEXT_LABELS);
    tt_node_ptr_t C = tt_trie_union_map(&a, A, B);
    tt_arena_flush(&a);
    tt_dump_arena_dot_styled("Cpredrop", C, &a, TT_STYLE_TEXT_LABELS);
    tt_drop(&a, A);
    tt_drop(&a, B);
    printf("\nC node: %u/%u\n", tt_ptr_idx(C), tt_ptr_tag(C));
    tt_arena_flush(&a);
    tt_dump_arena_dot_styled("Cpostdrop", C, &a, TT_STYLE_TEXT_LABELS);
    putchar('\n');
    tt_dump_routingtable(&a, C, 0);
    tt_drop(&a, C);
  }

  printf("\n============================================================ 4\n");
  {
    tt_node_ptr_t A = tt_grab(&a, MAKE_PATH(&a, b_set /* !!! */, {TT_BOS, 'A', TT_EOS}));
    printf("\nA node: %u/%u\n", tt_ptr_idx(A), tt_ptr_tag(A));
    tt_arena_flush(&a);
    tt_dump_arena_dot_styled("A", A, &a, TT_STYLE_TEXT_LABELS);
    tt_node_ptr_t B = tt_grab(&a, MAKE_PATH(&a, b_set, {TT_WILD}));
    printf("\nB node: %u/%u\n", tt_ptr_idx(B), tt_ptr_tag(B));
    tt_dump_arena_dot_styled("B", B, &a, TT_STYLE_TEXT_LABELS);
    tt_node_ptr_t C = tt_trie_union_map(&a, A, B);
    tt_arena_flush(&a);
    tt_dump_arena_dot_styled("Cpredrop", C, &a, TT_STYLE_TEXT_LABELS);
    tt_drop(&a, A);
    tt_drop(&a, B);
    printf("\nC node: %u/%u\n", tt_ptr_idx(C), tt_ptr_tag(C));
    tt_arena_flush(&a);
    tt_dump_arena_dot_styled("Cpostdrop", C, &a, TT_STYLE_TEXT_LABELS);
    putchar('\n');
    tt_dump_routingtable(&a, C, 0);
    tt_drop(&a, C);
  }

  printf("\n============================================================ 5\n");
  {
    tt_node_ptr_t A = tt_grab(&a, MAKE_PATH(&a, a_set, {TT_BOS, 'A', 'B', TT_EOS}));
    printf("\nA node: %u/%u\n", tt_ptr_idx(A), tt_ptr_tag(A));
    tt_arena_flush(&a);
    tt_dump_arena_dot_styled("A", A, &a, TT_STYLE_TEXT_LABELS);
    tt_node_ptr_t B = tt_grab(&a, MAKE_PATH(&a, b_set, {TT_BOS, TT_WILD, TT_EOS}));
    printf("\nB node: %u/%u\n", tt_ptr_idx(B), tt_ptr_tag(B));
    tt_dump_arena_dot_styled("B", B, &a, TT_STYLE_TEXT_LABELS);
    tt_node_ptr_t C = tt_trie_union_map(&a, A, B);
    tt_arena_flush(&a);
    tt_dump_arena_dot_styled("Cpredrop", C, &a, TT_STYLE_TEXT_LABELS);
    tt_drop(&a, A);
    tt_drop(&a, B);
    printf("\nC node: %u/%u\n", tt_ptr_idx(C), tt_ptr_tag(C));
    tt_arena_flush(&a);
    tt_dump_arena_dot_styled("Cpostdrop", C, &a, TT_STYLE_TEXT_LABELS);
    putchar('\n');
    tt_dump_routingtable(&a, C, 0);
    tt_drop(&a, C);
  }

  printf("\n============================================================ 6\n");
  {
    tt_node_ptr_t A = tt_grab(&a, MAKE_PATH(&a, a_set, {TT_BOS, 'A', 'B', TT_EOS}));
    printf("\nA node: %u/%u\n", tt_ptr_idx(A), tt_ptr_tag(A));
    tt_arena_flush(&a);
    tt_dump_arena_dot_styled("A", A, &a, TT_STYLE_TEXT_LABELS);
    tt_node_ptr_t B = tt_grab(&a, MAKE_PATH(&a, b_set, {TT_BOS, TT_WILD, TT_WILD, TT_EOS}));
    printf("\nB node: %u/%u\n", tt_ptr_idx(B), tt_ptr_tag(B));
    tt_dump_arena_dot_styled("B", B, &a, TT_STYLE_TEXT_LABELS);
    tt_node_ptr_t C = tt_trie_union_map(&a, A, B);
    tt_arena_flush(&a);
    tt_dump_arena_dot_styled("Cpredrop", C, &a, TT_STYLE_TEXT_LABELS);
    tt_drop(&a, A);
    tt_drop(&a, B);
    printf("\nC node: %u/%u\n", tt_ptr_idx(C), tt_ptr_tag(C));
    tt_arena_flush(&a);
    tt_dump_arena_dot_styled("Cpostdrop", C, &a, TT_STYLE_TEXT_LABELS);
    putchar('\n');
    tt_dump_routingtable(&a, C, 0);
    tt_drop(&a, C);
  }

  printf("\n============================================================ 7\n");
  {
    tt_node_ptr_t A = tt_grab(&a, MAKE_PATH(&a, a_set, {TT_BOS, 'A', TT_WILD, TT_EOS}));
    printf("\nA node: %u/%u\n", tt_ptr_idx(A), tt_ptr_tag(A));
    tt_arena_flush(&a);
    tt_dump_arena_dot_styled("A", A, &a, TT_STYLE_TEXT_LABELS);
    tt_node_ptr_t B = tt_grab(&a, MAKE_PATH(&a, b_set, {TT_BOS, TT_WILD, 'B', TT_EOS}));
    printf("\nB node: %u/%u\n", tt_ptr_idx(B), tt_ptr_tag(B));
    tt_dump_arena_dot_styled("B", B, &a, TT_STYLE_TEXT_LABELS);
    tt_node_ptr_t C = tt_trie_union_map(&a, A, B);
    tt_arena_flush(&a);
    tt_dump_arena_dot_styled("Cpredrop", C, &a, TT_STYLE_TEXT_LABELS);
    tt_drop(&a, A);
    tt_drop(&a, B);
    printf("\nC node: %u/%u\n", tt_ptr_idx(C), tt_ptr_tag(C));
    tt_arena_flush(&a);
    tt_dump_arena_dot_styled("Cpostdrop", C, &a, TT_STYLE_TEXT_LABELS);
    putchar('\n');
    tt_dump_routingtable(&a, C, 0);

    tt_node_ptr_t D = tt_trie_union_map(&a, C, C);
    tt_drop(&a, C);
    tt_arena_flush(&a);
    tt_dump_arena_dot_styled("D", D, &a, TT_STYLE_TEXT_LABELS);
    tt_dump_routingtable(&a, D, 0);

    tt_node_ptr_t E = tt_grab(&a, MAKE_PATH(&a, b_set, {TT_WILD}));
    tt_arena_flush(&a);
    tt_dump_arena_dot_styled("E", E, &a, TT_STYLE_TEXT_LABELS);
    tt_node_ptr_t F = tt_trie_union_map(&a, D, E);
    tt_drop(&a, D);
    tt_drop(&a, E);
    tt_arena_flush(&a);
    tt_dump_arena_dot_styled("F", F, &a, TT_STYLE_TEXT_LABELS);
    tt_dump_routingtable(&a, F, 0);

    tt_node_ptr_t G = tt_grab(&a, tt_trie_relabel_const(&a, F, a_set));
    tt_drop(&a, F);
    tt_arena_flush(&a);
    tt_dump_arena_dot_styled("G", G, &a, TT_STYLE_TEXT_LABELS);
    tt_dump_routingtable(&a, G, 0);
    tt_drop(&a, G);
  }

  printf("\n============================================================ 8\n");
  {
    tt_node_ptr_t A = tt_grab(&a, MAKE_PATH(&a, TT_EMPTY_DICT, {TT_BOS, 'A', TT_WILD, TT_EOS}));
    printf("\nA node: %u/%u\n", tt_ptr_idx(A), tt_ptr_tag(A));
    tt_arena_flush(&a);
    tt_dump_arena_dot_styled("A", A, &a, TT_STYLE_TEXT_LABELS);
    tt_dump_routingtable(&a, A, 0);
    tt_node_ptr_t B = tt_grab(&a, MAKE_PATH(&a, TT_EMPTY_DICT, {TT_BOS, TT_WILD, 'B', TT_EOS}));
    printf("\nB node: %u/%u\n", tt_ptr_idx(B), tt_ptr_tag(B));
    tt_dump_arena_dot_styled("B", B, &a, TT_STYLE_TEXT_LABELS);
    tt_dump_routingtable(&a, B, 0);
    tt_node_ptr_t C = tt_trie_subtract_set(&a, A, B);
    tt_arena_flush(&a);
    tt_dump_arena_dot_styled("Cpredrop", C, &a, TT_STYLE_TEXT_LABELS);
    tt_drop(&a, A);
    tt_drop(&a, B);
    printf("\nC node: %u/%u\n", tt_ptr_idx(C), tt_ptr_tag(C));
    tt_arena_flush(&a);
    tt_dump_arena_dot_styled("Cpostdrop", C, &a, TT_STYLE_TEXT_LABELS);
    putchar('\n');
    tt_dump_routingtable(&a, C, 0);
    tt_drop(&a, C);
  }

  printf("\n============================================================ 9\n");
  {
    tt_node_ptr_t A = tt_grab(&a, MAKE_PATH(&a, TT_EMPTY_DICT, {TT_WILD}));
    printf("\nA node: %u/%u\n", tt_ptr_idx(A), tt_ptr_tag(A));
    tt_arena_flush(&a);
    tt_dump_arena_dot_styled("A", A, &a, TT_STYLE_TEXT_LABELS);
    tt_dump_routingtable(&a, A, 0);
    tt_node_ptr_t B = tt_grab(&a, MAKE_PATH(&a, TT_EMPTY_DICT, {TT_BOS, 'A', 'B', TT_EOS}));
    printf("\nB node: %u/%u\n", tt_ptr_idx(B), tt_ptr_tag(B));
    tt_dump_arena_dot_styled("B", B, &a, TT_STYLE_TEXT_LABELS);
    tt_dump_routingtable(&a, B, 0);
    printf("(calling tt_trie_subtract_set now)\n");
    tt_node_ptr_t C = tt_trie_subtract_set(&a, A, B);
    tt_arena_flush(&a);
    tt_dump_arena_dot_styled("Cpredrop", C, &a, TT_STYLE_TEXT_LABELS);
    tt_drop(&a, A);
    tt_drop(&a, B);
    printf("\nC node: %u/%u\n", tt_ptr_idx(C), tt_ptr_tag(C));
    tt_arena_flush(&a);
    tt_dump_arena_dot_styled("Cpostdrop", C, &a, TT_STYLE_TEXT_LABELS | TT_STYLE_HIDE_DETAILS);
    tt_dump_routingtable(&a, C, 0);
    tt_replace(&a, &C, tt_trie_relabel_const(&a, C, a_set));
    printf("\nC node post relabel: %u/%u\n", tt_ptr_idx(C), tt_ptr_tag(C));
    tt_arena_flush(&a);
    tt_dump_arena_dot_styled("Cpostrelabel", C, &a, TT_STYLE_TEXT_LABELS | TT_STYLE_HIDE_DETAILS);
    putchar('\n');
    tt_dump_routingtable(&a, C, 0);
    tt_drop(&a, C);
  }

  printf("\n============================================================ A\n");
  {
    tt_node_ptr_t d = TT_EMPTY_DICT;
    tt_replace(&a, &d, tt_dict_set(&a, d, LITERAL4("appl"), TT_EMPTY));
    tt_replace(&a, &d, tt_dict_set(&a, d, LITERAL4("caar"), TT_EMPTY));
    tt_replace(&a, &d, tt_dict_set(&a, d, LITERAL4("cadr"), TT_EMPTY));
    tt_replace(&a, &d, tt_dict_set(&a, d, LITERAL4("cdar"), TT_EMPTY));
    tt_replace(&a, &d, tt_dict_set(&a, d, LITERAL4("cddr"), TT_EMPTY));
    tt_replace(&a, &d, tt_dict_set(&a, d, LITERAL4("cons"), TT_EMPTY));
    tt_replace(&a, &d, tt_dict_set(&a, d, LITERAL4("goog"), TT_EMPTY));
    tt_replace(&a, &d, tt_dict_set(&a, d, LITERAL4("lisp"), TT_EMPTY));
    tt_replace(&a, &d, tt_dict_set(&a, d, LITERAL4("snoc"), TT_EMPTY));
    tt_replace(&a, &d, tt_dict_set(&a, d, LITERAL4("spcx"), TT_EMPTY));
    tt_replace(&a, &d, tt_dict_set(&a, d, LITERAL4("twtr"), TT_EMPTY));
    tt_replace(&a, &d, tt_dict_set(&a, d, LITERAL4("unit"), TT_EMPTY));
    tt_replace(&a, &d, tt_dict_set(&a, d, LITERAL4("void"), TT_EMPTY));
    printf("\nd node: %u/%u\n", tt_ptr_idx(d), tt_ptr_tag(d));
    tt_arena_flush(&a);
    tt_dump_arena_dot_styled("d", d, &a, TT_STYLE_TEXT_LABELS | TT_STYLE_HIDE_DETAILS);
    tt_replace(&a, &d, tt_dict_remove(&a, d, LITERAL4("goog")));
    printf("\ndPostRemove node: %u/%u\n", tt_ptr_idx(d), tt_ptr_tag(d));
    tt_arena_flush(&a);
    tt_dump_arena_dot_styled("dPostRemove", d, &a, TT_STYLE_TEXT_LABELS | TT_STYLE_HIDE_DETAILS);
    tt_drop(&a, d);
  }

  printf("\n============================================================ B\n");
  {
    tt_node_ptr_t A = tt_grab(&a, MAKE_PATH(&a, TT_EMPTY_DICT, {TT_BOS, 'A', TT_WILD, TT_EOS}));
    printf("\nA node: %u/%u\n", tt_ptr_idx(A), tt_ptr_tag(A));
    tt_arena_flush(&a);
    tt_dump_arena_dot_styled("A", A, &a, TT_STYLE_TEXT_LABELS);
    tt_dump_routingtable(&a, A, 0);
    tt_node_ptr_t B = tt_grab(&a, MAKE_PATH(&a, TT_EMPTY_DICT, {TT_BOS, TT_WILD, 'B', TT_EOS}));
    printf("\nB node: %u/%u\n", tt_ptr_idx(B), tt_ptr_tag(B));
    tt_dump_arena_dot_styled("B", B, &a, TT_STYLE_TEXT_LABELS);
    tt_dump_routingtable(&a, B, 0);
    tt_node_ptr_t C = tt_trie_union_set(&a, A, B);
    tt_arena_flush(&a);
    tt_dump_arena_dot_styled("Cpredrop", C, &a, TT_STYLE_TEXT_LABELS);
    tt_drop(&a, A);
    tt_drop(&a, B);
    printf("\nC node: %u/%u\n", tt_ptr_idx(C), tt_ptr_tag(C));
    tt_arena_flush(&a);
    tt_dump_arena_dot_styled("Cpostdrop", C, &a, TT_STYLE_TEXT_LABELS);
    putchar('\n');
    tt_dump_routingtable(&a, C, 0);
    tt_drop(&a, C);
  }

  printf("\n============================================================ C\n");
  {
#define N_PATTERNS 10000
#define N_MESSAGES 10000000
    tt_node_ptr_t r = TT_EMPTY;
    struct timeval start_time, stop_time;
    uint64_t start_cycles, stop_cycles;
    int i;

    gettimeofday(&start_time, NULL);
    for (i = 0; i < N_PATTERNS; i++) {
      tt_atom_t key = (i << 1) | 1;
      tt_node_ptr_t d = tt_grab(&a, tt_dict_singleton(&a, key, TT_EMPTY));
      tt_node_ptr_t p = tt_grab(&a, MAKE_PATH(&a, d, {TT_BOS, key, TT_WILD, TT_EOS}));
      tt_replace(&a, &r, tt_trie_union_map(&a, r, p));
      tt_drop(&a, d);
      tt_drop(&a, p);
      tt_drop(&a, r); /* because tt_trie_union_map returns grab'd */
    }
    gettimeofday(&stop_time, NULL);
    {
      double delta_ms = (stop_time.tv_sec - start_time.tv_sec) * 1000.0 +
	(stop_time.tv_usec - start_time.tv_usec) / 1000.0;
      printf("Construction time %g ms; that is, %g microsec per pattern, %.12g Hz\n",
	     delta_ms,
	     1000.0 * delta_ms / N_PATTERNS,
	     N_PATTERNS / (delta_ms / 1000.0));
    }

    printf("r node: %u/%u\n", tt_ptr_idx(r), tt_ptr_tag(r));
    /* tt_arena_flush(&a); */
    /* tt_dump_arena_dot_styled("r", r, &a, TT_STYLE_TEXT_LABELS /\* | TT_STYLE_HIDE_DETAILS *\/); */

    gettimeofday(&start_time, NULL);
    start_cycles = rdtsc();
    for (i = 0; i < N_MESSAGES; i++) {
      tt_atom_t key = ((rand() % N_PATTERNS) << 1) | 1;
      tt_node_ptr_t p = r;
      p = tt_trie_step(&a, p, TT_BOS);
      p = tt_trie_step(&a, p, key);
      p = tt_trie_step(&a, p, key);
      p = tt_trie_step(&a, p, TT_EOS);
      assert(tt_ptr_tag(p) == TT_TAG_OK);
      /* printf("\nstepped: key %d, %u/%u\n", key, tt_ptr_idx(p), tt_ptr_tag(p)); */
      /* tt_dump_arena_dot_styled("result", p, &a, TT_STYLE_TEXT_LABELS); */
    }
    stop_cycles = rdtsc();
    gettimeofday(&stop_time, NULL);
    {
      double delta_ms = (stop_time.tv_sec - start_time.tv_sec) * 1000.0 +
	(stop_time.tv_usec - start_time.tv_usec) / 1000.0;
      printf("Routing time %g ms; that is, %g microsec per routed message, %.12g Hz\n",
	     delta_ms,
	     1000.0 * delta_ms / N_MESSAGES,
	     N_MESSAGES / (delta_ms / 1000.0));
      printf("%lld cycles elapsed; %g cycles per routed message\n",
	     ((long long int) stop_cycles - (long long int) start_cycles),
	     (double) (stop_cycles - start_cycles) / N_MESSAGES);
    }

    tt_drop(&a, r);
    printf("\n");
#undef N_PATTERNS
#undef N_MESSAGES
  }

  tt_arena_flush(&a);
  tt_dump_arena_dot_styled("afterAll", TT_NO_PTR, &a, TT_STYLE_TEXT_LABELS); /* expect a_set and b_set here */

  tt_arena_done(&a);
  return EXIT_SUCCESS;
}
