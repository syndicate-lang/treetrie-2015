all: t

t: *.c
	$(CC) -Wall -Os -o $@ -g *.c

clean:
	rm -f t
