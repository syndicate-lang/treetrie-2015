#ifndef CRITBIT_H_2809050e_f3c9_4363_8208_673da32e70f9
#define CRITBIT_H_2809050e_f3c9_4363_8208_673da32e70f9

#ifdef __cplusplus
extern "C" {
#endif

extern int tt_dict_size(tt_arena_t *a, tt_node_ptr_t t);

/* Returns TT_NO_PTR when key not present. Does not manipulate references. */
extern tt_node_ptr_t tt_dict_get(tt_arena_t *a, tt_node_ptr_t t, tt_atom_t key);

/* Returns TT_NO_PTR when allocation failed. Otherwise, a length-one dict.
   Grabs `trie` if required. */
extern tt_node_ptr_t tt_dict_singleton(tt_arena_t *a,
				       tt_atom_t key,
				       tt_node_ptr_t trie);

/* Returns TT_NO_PTR when allocation failed. Otherwise, yields a dict.
   Grabs `trie` if required. */
extern tt_node_ptr_t tt_dict_set(tt_arena_t *a,
				 tt_node_ptr_t t,
				 tt_atom_t key,
				 tt_node_ptr_t trie);

extern tt_node_ptr_t tt_dict_remove(tt_arena_t *a,
				    tt_node_ptr_t t,
				    tt_atom_t key);

/* Returns 1 if it examined the whole dict; 0 if it was aborted early */
extern int tt_dict_foreach(tt_arena_t *a,
			   tt_node_ptr_t t,
			   void *context,
			   /* Return 1 to continue the loop; 0 to abort it. */
			   int (*f)(void *, tt_atom_t key, tt_node_ptr_t trie));

/* Dictsets map to TT_EMPTY to indicate a *present* key (and
   implicitly to TT_NO_PTR to indicate an *absent* key). */
/* THESE ALL RETURN grab'd RESULTS */
extern tt_node_ptr_t tt_dictset_union(tt_arena_t *a, tt_node_ptr_t s1, tt_node_ptr_t s2);
extern tt_node_ptr_t tt_dictset_intersection(tt_arena_t *a, tt_node_ptr_t s1, tt_node_ptr_t s2);
extern tt_node_ptr_t tt_dictset_difference(tt_arena_t *a, tt_node_ptr_t s1, tt_node_ptr_t s2);

#ifdef __cplusplus
}
#endif

#endif
